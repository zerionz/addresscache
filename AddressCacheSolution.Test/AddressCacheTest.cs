﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AddressCacheSolution.Test
{
    [TestClass]
    public class AddressCacheTest
    {
        [TestMethod]
        public void AddressCache_Add()
        {
            AddressCache<string> cache = new AddressCache<string>(DateTimeOffset.MaxValue);

            Assert.AreEqual(cache.Add("ZZZZ"), true);
            Assert.AreEqual(cache.Add(null), false);

            var expect = "AAAA";
            cache.Add(expect);
            Assert.AreEqual(cache.Add(expect), false);
            Assert.AreEqual(expect, cache.Peek());
        }

        [TestMethod]
        public void AddressCache_Remove()
        {
            AddressCache<string> cache = new AddressCache<string>(DateTimeOffset.MaxValue);

            var expect = "BBBB";
            cache.Add("AAAA");
            cache.Add(expect);

            Assert.AreEqual(cache.Remove(expect), true);
            Assert.AreEqual(cache.Remove(null), false);
        }

        [TestMethod]
        public void AddressCache_Peek()
        {
            AddressCache<string> cache = new AddressCache<string>(DateTimeOffset.MaxValue);

            Assert.AreEqual(cache.Peek(), null);

            cache.Add("AAAA");
            cache.Add("BBBB");
            cache.Add("CCCC");

            Assert.AreEqual(cache.Peek(), "CCCC");
        }

        [TestMethod]
        public void AddressCache_Take()
        {
            AddressCache<string> cache = new AddressCache<string>(DateTimeOffset.MaxValue);

            cache.Add("AAAA");
            cache.Add("BBBB");
            cache.Add("CCCC");

            Assert.AreEqual(cache.Take(), "CCCC");
            Assert.AreEqual(cache.Take(), "BBBB");
            Assert.AreEqual(cache.Take(), "AAAA");
            Assert.AreEqual(cache.Take(), null);
        }
    }
}
