﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace AddressCacheSolution
{
    public class AddressCache<T> : CacheServiceBase, IStackHashCache<T> where T : class
    {
        private LinkedList<string> _indexer;

        private static readonly object _lock = new object();

        /// <summary>
        /// Constructor for initialized indexer by LinkedList
        /// </summary>
        /// <param name="maxAge">Age of cache data.</param>
        public AddressCache(DateTimeOffset maxAge)
            : base(maxAge)
        {
            _indexer = new LinkedList<string>();
        }

        /// <summary>
        /// Method will use hashCode for store unique elements.
        /// This will return true if the element was successfully added. 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Add(T value)
        {
            if (value != null)
            {
                var hash = value.GetHashCode().ToString();
                if (base.AddCache(hash, value))
                {
                    lock (_lock)
                    {
                        _indexer.AddFirst(hash);
                    }
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Method will return true if the address was successfully removed
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Remove(T value)
        {
            if (value != null)
            {
                lock (_lock)
                {
                    var hash = value.GetHashCode().ToString();
                    if (base.RemoveCache(hash))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Method will return the most recently added element, null if no element exists.
        /// </summary>
        /// <returns></returns>
        public T Peek()
        {
            lock (_lock)
            {
                var topHash = _indexer.FirstOrDefault();
                if (topHash != null)
                {
                    var result = base.GetCache(topHash, false);
                    if (result != null)
                    {
                        return (T)result;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Method retrieves and removes the most recently added element from the cache and waits if necessary until an element becomes available.
        /// </summary>
        /// <returns></returns>
        public T Take()
        {
            lock (_lock)
            {
                var topHash = _indexer.FirstOrDefault();
                if (topHash != null)
                {
                    var result = base.GetCache(topHash, true);
                    if (result != null)
                    {
                        return (T)result;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// This is callback event that fires when cache item expired or remove.
        /// </summary>
        /// <param name="arguments"></param>
        protected override void CacheRemovedCallback(CacheEntryRemovedArguments arguments)
        {
            lock (_lock)
            {
                if (arguments != null && arguments.CacheItem != null && arguments.CacheItem.Key != null)
                {
                    _indexer.Remove(arguments.CacheItem.Key);
                }
            }
        }
    }
}
