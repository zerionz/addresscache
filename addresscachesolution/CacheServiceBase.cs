﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace AddressCacheSolution
{
    public abstract class CacheServiceBase
    {
        protected MemoryCache cacheService;

        protected DateTimeOffset age;

        private static readonly object _lock = new object();

        /// <summary>
        /// Constructor for initialized MemoryCache class to manage cache items and expiration.
        /// </summary>
        /// <param name="maxAge"></param>
        public CacheServiceBase(DateTimeOffset maxAge)
        {
            this.age = maxAge;
            cacheService = new MemoryCache("InMemCache");
            DeleteLogFiles();
        }

        /// <summary>
        /// This method will return CacheItemPolicy with expiry time and removeCallback event.
        /// </summary>
        /// <returns></returns>
        private CacheItemPolicy GetPolicy()
        {
            var policy = new CacheItemPolicy();
            policy.AbsoluteExpiration = this.age;
            policy.RemovedCallback = new CacheEntryRemovedCallback(this.CacheRemovedCallback);
            return policy;
        }

        /// <summary>
        /// This method will called when cache items expired or removed.
        /// </summary>
        /// <param name="arguments"></param>
        protected virtual void CacheRemovedCallback(CacheEntryRemovedArguments arguments)
        {

        }

        /// <summary>
        /// This method will add object to MemoryCache by key,
        /// return true if added and false when found existed key.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected virtual bool AddCache(string key, object value)
        {
            lock (_lock)
            {
                if (cacheService[key] == null)
                {
                    cacheService.Add(key, value, GetPolicy());
                    return true;
                }
                else
                {
                    WriteErrorLog("CacheServiceBase: Add cache by contains key " + key);
                    return false;
                }
            }
        }

        /// <summary>
        /// This method will remove object from MemoryCache by key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected virtual bool RemoveCache(string key)
        {
            lock (_lock)
            {
                var result = cacheService.Remove(key);
                if (result != null)
                {
                    return true;
                }
                else
                {
                    WriteErrorLog("CacheServiceBase: Remove cache by contains key " + key);
                    return false;
                }
            }
        }

        /// <summary>
        /// This method will return cache item from key and remove if need.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="needRemove"></param>
        /// <returns></returns>
        protected virtual object GetCache(string key, bool needRemove)
        {
            lock (_lock)
            {
                var result = cacheService[key];
                if (result != null)
                {
                    if (needRemove)
                    {
                        cacheService.Remove(key);
                    }
                }
                else
                {
                    WriteErrorLog("CacheServiceBase: Get cache by didn't contains key " + key);
                }
                return result;
            }
        }

        #region Logs

        string LogPath = System.Environment.GetEnvironmentVariable("TEMP");

        protected void WriteErrorLog(string text)
        {
            using (System.IO.TextWriter tw = System.IO.File.AppendText(string.Format("{0}\\CacheServiceBaseErrors.txt", LogPath)))
            {
                tw.WriteLine(text);
                tw.Close();
            }
        }

        protected void DeleteLogFiles()
        {
            System.IO.File.Delete(string.Format("{0}\\CacheServiceBaseErrors.txt", LogPath));
        }

        #endregion Logs
    }
}
