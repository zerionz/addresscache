﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddressCacheSolution
{
    public interface IStackHashCache<T> where T : class
    {
        bool Add(T value);

        bool Remove(T value);

        T Peek();

        T Take();
    }
}
